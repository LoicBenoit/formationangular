import { Injectable } from '@angular/core';
import {Task} from "./task";

@Injectable({
  providedIn: 'root'
})
export class TaskService {


  tasks: Task[] = [
    {title: 'yepito', checked: false},
    {title: 'blueblue', checked: true},
    {title: 'redisred', checked: false}
  ];

  constructor() { }


  addTask(taskToAdd: Task){
    if (taskToAdd.title && taskToAdd.title !== '') {
      this.tasks.push(taskToAdd)
    }
  }
  check(task: Task) {
    task.checked = !task.checked;
    let taskToUpdate = this.tasks.find(it => it.title === task.title)
    if (taskToUpdate) {
      taskToUpdate.checked = task.checked;
    }
  }

  delete(task: Task) {
    const index = this.tasks.indexOf(task, 0);
    if (index > -1) {
      this.tasks.splice(index, 1);
    }
  }

}
