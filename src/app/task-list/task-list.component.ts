import {Component, Input, Output} from '@angular/core';
import {Task} from "../task";
import {TaskService} from "../task.service";

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent {

  tasks : Task[] = [];
  constructor(private taskService : TaskService) {
    this.tasks = taskService.tasks;
  }


  check (task : Task) {
    this.taskService.check(task);
  }

  delete (task : Task) {
    this.taskService.delete(task);
  }

}
