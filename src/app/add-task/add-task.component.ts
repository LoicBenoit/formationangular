import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Task} from '../task';
import {TaskService} from "../task.service";

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent {

  task: Task = {title: '', checked: false};

  constructor(private taskService : TaskService) {}

  addTask(): void {
    this.taskService.addTask(this.task);
  }
}
